﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmGestionEmpleado : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;

        public FrmGestionEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataSet DsEmpleados { get => dsEmpleados; set => dsEmpleados = value; }
        public BindingSource BsEmpleados { get => bsEmpleados; set => bsEmpleados = value; }

        private void FrmGestionEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsEmpleados.Filter = string.Format("Nombre like '*{0}*' or Apellido like '*{0}*'",
                    txtBuscar.Text);
            }
            catch (InvalidExpressionException)
            {

            }
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmEmpleado fe = new FrmEmpleado();
            fe.DtEmpleados = dsEmpleados.Tables["Empleado"];
            fe.Show();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvEmpleados.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Para editar un empleado, primero debe seleccionarlo.",
                    "Mensaje de error", MessageBoxButtons.OK);
                return;
            }

            DataGridViewRow fila = seleccionados[0];
            DataRow drEdita = ((DataRowView)fila.DataBoundItem).Row;

            FrmEmpleado fe = new FrmEmpleado();
            fe.DtEmpleados = dsEmpleados.Tables["Empleado"];
            fe.DrEmpleados = drEdita;
            fe.Show();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvEmpleados.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Para eliminar un empleado, primero debe seleccionarlo.",
                    "Mensaje de error", MessageBoxButtons.OK);
                return;
            }

            DataGridViewRow fila = seleccionados[0];
            DataRow eliminar = ((DataRowView)fila.DataBoundItem).Row;

            DialogResult respuesta = MessageBox.Show(this, "¿Está seguro que desea eliminar este empleado?",
                "Mensaje de confirmación", MessageBoxButtons.OKCancel);

            if(respuesta == DialogResult.OK)
            {
                dsEmpleados.Tables["Empleado"].Rows.Remove(eliminar);
            }
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
