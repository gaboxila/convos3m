﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmGestionProductos : Form
    {

        private DataSet dsProductos;
        private BindingSource bsProductos;

        public FrmGestionProductos()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

        public DataSet DsProductos { get => dsProductos; set => dsProductos = value; }
        public BindingSource BsProductos { get => bsProductos; set => bsProductos = value; }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsProductos.Filter = string.Format("Marca like '*{0}*' or Modelo like'*{0}*'", txtBuscar.Text);
            }
            catch (InvalidExpressionException)
            {

            }
        }

        private void FrmGestionProductos_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = dsProductos;
            bsProductos.DataMember = dsProductos.Tables["Producto"].TableName;
            dgvProductos.DataSource = bsProductos;
            dgvProductos.AutoGenerateColumns = true;
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmProducto fp = new FrmProducto();
            fp.DtProductos = dsProductos.Tables["Producto"];
            fp.Show();
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvProductos.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Para editar un producto, primero tiene que seleccinarlo",
                    "Mensaje de error", MessageBoxButtons.OK);
                return;
            }

            DataGridViewRow fila = seleccionados[0];
            DataRow drNuevo = ((DataRowView)fila.DataBoundItem).Row;

            FrmProducto fp = new FrmProducto();
            fp.DtProductos = dsProductos.Tables["Producto"];
            fp.DrProductos = drNuevo;
            fp.Show();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvProductos.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar un producto si quiere eliminarlo",
                    "Mensaje de error", MessageBoxButtons.OK);
            }

            DataGridViewRow fila = seleccionados[0];
            DataRow drElimina = ((DataRowView)fila.DataBoundItem).Row;

            DialogResult respuesta = MessageBox.Show(this, "¿Está seguro de que quiere eliminar el producto?",
                "Mensaje de confirmación", MessageBoxButtons.OKCancel);
            
            if(respuesta == DialogResult.OK)
            {
                DsProductos.Tables["Producto"].Rows.Remove(drElimina);
            }
        }

        private void FrmGestionProductos_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DataTable agregados = dsProductos.Tables["Producto"].GetChanges(DataRowState.Added);

            //foreach (DataRow dr in agregados.Rows)
            //{
            //    //controla.guardar(dr);   
            //    dsProductos.Tables["Producto"].Rows.Find(dr["Id"]).AcceptChanges();
            //}
        }
    }
}
