﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void ProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsSistema;
            fgp.Show();
        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleado fge = new FrmGestionEmpleado();
            fge.MdiParent = this;
            fge.DsEmpleados = dsSistema;
            fge.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionCliente fgc = new FrmGestionCliente();
            fgc.MdiParent = this;
            fgc.DsClientes = dsSistema;
            fgc.Show();
        }

        private void NuevaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsFactura = dsSistema;
            ff.Show();
        }
    }
}
