﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmCliente : Form
    {
        private DataTable dtClientes;
        private DataRow drCliente;

        public FrmCliente()
        {
            InitializeComponent();
        }

        public DataTable DtClientes { get => dtClientes; set => dtClientes = value; }
        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                txtNombre.Text = drCliente["Nombre"].ToString();
                txtApellido.Text = drCliente["Apellido"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
                txtTelefono.Text = drCliente["Telefono"].ToString();
                drCliente["NA"] = drCliente["Nombre"] + " " + drCliente["Apellido"];
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            string nombre, apellido, correo, direccion, telefono, NA;

            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            correo = txtCorreo.Text;
            direccion = txtDireccion.Text;
            telefono = txtTelefono.Text;
            NA = nombre + " " + apellido;
            if(drCliente != null)
            {
                DataRow drEdita = dtClientes.NewRow();

                int indice = dtClientes.Rows.IndexOf(drCliente);

                drEdita["Id"] = drCliente["Id"];
                drEdita["Nombre"] = nombre;
                drEdita["Apellido"] = apellido;
                drEdita["Correo"] = correo;
                drEdita["Direccion"] = direccion;
                drEdita["Telefono"] = telefono;
                drEdita["NA"] = NA;
                dtClientes.Rows.RemoveAt(indice);
                dtClientes.Rows.InsertAt(drEdita, indice);
                dtClientes.Rows[indice].AcceptChanges();
                dtClientes.Rows[indice].SetModified();
            }
            else
            {
                dtClientes.Rows.Add(dtClientes.Rows.Count + 1, nombre, apellido, correo, 
                    direccion, telefono, NA);
            }
            Dispose();
        }
    }
}
