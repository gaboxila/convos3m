﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmEmpleado : Form
    {
        private DataTable dtEmpleados;
        private DataRow drEmpleados;

        public FrmEmpleado()
        {
            InitializeComponent();
        }

        public DataTable DtEmpleados { get => dtEmpleados; set => dtEmpleados = value; }
        public DataRow DrEmpleados
        {
            set
            {
                drEmpleados = value;
                txtId.Text = drEmpleados["Id"].ToString();
                txtCedula.Text = drEmpleados["Cedula"].ToString();
                txtNombres.Text = drEmpleados["Nombre"].ToString();
                txtApellidos.Text = drEmpleados["Apellido"].ToString();
                txtEdad.Text = drEmpleados["Edad"].ToString();
                cmbSexo.SelectedItem = drEmpleados["Sexo"].ToString();
                drEmpleados["NA"] = drEmpleados["Nombre"].ToString() + " " + drEmpleados["Apellidos"].ToString();
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            string cedula, nombre, apellido, sexo, edad, NA;

            cedula = txtCedula.Text;
            nombre = txtNombres.Text;
            apellido = txtApellidos.Text;
            edad = txtEdad.Text;
            sexo = cmbSexo.SelectedItem.ToString();
            NA = nombre + " " + apellido;
            if(drEmpleados != null)
            {
                DataRow drEdita = dtEmpleados.NewRow();
                int indice = DtEmpleados.Rows.IndexOf(drEmpleados);
                drEdita["Id"] = drEmpleados["Id"];
                drEdita["Cedula"] = cedula;
                drEdita["Nombre"] = nombre;
                drEdita["Apellido"] = apellido;
                drEdita["Edad"] = edad;
                drEdita["Sexo"] = sexo;
                drEdita["NA"] = NA;
                DtEmpleados.Rows.RemoveAt(indice);
                DtEmpleados.Rows.InsertAt(drEdita, indice);
                DtEmpleados.Rows[indice].AcceptChanges();
                DtEmpleados.Rows[indice].SetModified();
            }
            else
            {
                dtEmpleados.Rows.Add(dtEmpleados.Rows.Count + 1, cedula, nombre, apellido, edad, sexo, NA);
            }
            Dispose();
        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            int id = dtEmpleados.Rows.Count + 1;
            txtId.Text = id.ToString();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
