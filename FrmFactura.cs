﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmFactura : Form
    {
        private DataSet dsFactura;
        private BindingSource bsFactura;

        public FrmFactura()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
        }

        public DataSet DsFactura
        {
            set
            {
                dsFactura = value;  
            }
        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleado.DataSource = dsFactura.Tables["Empleado"];
            cmbEmpleado.DisplayMember = "NA";
            cmbEmpleado.ValueMember = "Id";

            cmbCliente.DataSource = dsFactura.Tables["Cliente"];
            cmbCliente.DisplayMember = "NA";
            cmbCliente.ValueMember = "Id";

            cmbProducto.DataSource = dsFactura.Tables["Producto"];
            cmbProducto.DisplayMember = "Descripcion";
            cmbProducto.ValueMember = "Id";

            bsFactura.DataSource = dsFactura.Tables["ProductoFactura"];
            dgvProductosFactura.DataSource = bsFactura;
            dgvProductosFactura.AutoGenerateColumns = true;

            lblFecha.Text = DateTime.Now.ToString();
            int cuenta = dsFactura.Tables["Factura"].Rows.Count + 1;
            lblFactura.Text = "FA" + cuenta;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void CmbProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drCombo = ((DataRowView)cmbProducto.SelectedItem).Row;
            txtCantidad.Text = drCombo["Cantidad"].ToString();
            txtPrecio.Text = drCombo["Precio"].ToString();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.DtClientes = dsFactura.Tables["Cliente"];
            fc.Show();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            //if (cmbCliente.SelectedIndex < 0)
            //{
            //    MessageBox.Show(this, "Mensaje de error", "El campo cliente no puede estar vacío",
            //        MessageBoxButtons.OK);
            //    return;
            //}

            //if (cmbEmpleado.SelectedIndex < 0)
            //{
            //    MessageBox.Show(this, "Mensaje de error", "El campo empleado no puede estar vacío",
            //        MessageBoxButtons.OK);
            //    return;
            //}

            if (cmbProducto.SelectedIndex < 0)
            {
                MessageBox.Show(this, "No ha seleccionado ningún producto", "Mensaje de error",
                    MessageBoxButtons.OK);
                return;
            }

            try
            {
                DataRow drProducto = ((DataRowView)cmbProducto.SelectedItem).Row;
                DataRow drProductoFactura = dsFactura.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["Nombre"] = drProducto["Descripcion"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Subtotal"] = drProducto["Precio"];
                double precio = Double.Parse(drProducto["Precio"].ToString());
                drProductoFactura["IVA"] = precio * 0.15;
                dsFactura.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
                calcularTotalFactura();
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "¡El producto ya fue agregado!", "Mensaje de error",
                    MessageBoxButtons.OK);
                return;
            }
        }

        private void calcularTotalFactura()
        {
            double subtotal = 0;

            foreach (DataRow dr in dsFactura.Tables["ProductoFactura"].Rows)
            {
                subtotal += Double.Parse(dr["Cantidad"].ToString())*Double.Parse(dr["Subtotal"].ToString());
            }

            txtSubtotal.Text = subtotal.ToString();
            txtIva.Text = (subtotal * 0.15).ToString();
            txtTotal.Text = (subtotal + (subtotal * 0.15)).ToString();

        }

        private void DgvProductosFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            calcularTotalFactura();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionado = dgvProductosFactura.SelectedRows;

            if(seleccionado.Count == 0)
            {
                MessageBox.Show(this, "Para eliminar un producto de la factura, debe seleccionarlo",
                    "Mensaje de información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            DataRow eliminar = ((DataRowView)seleccionado[0].DataBoundItem).Row;
            dsFactura.Tables["ProductoFactura"].Rows.Remove(eliminar);
            calcularTotalFactura();
        }

        private void BtnFacturar_Click(object sender, EventArgs e)
        {
            if (dsFactura.Tables["ProductoFactura"].Rows.Count == 0)
            {
                MessageBox.Show(this, "ERROR, No se puede generar la Factura, revise que hayan productos",
                    "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsFactura.Tables["Factura"].NewRow();
            drFactura["CodFactura"] = cod_factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtObserv.Text;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["SubTotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;
            drFactura["Cliente"] = cmbClientes.SelectedValue;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);


            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);

                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) -
                    Double.Parse(dr["Cantidad"].ToString());
            }

            FrmReporteFactura frf = new FrmReporteFactura();
            frf.MdiParent = this.MdiParent;
            frf.DsSistema = dsSistema;
            frf.Show();

            Dispose();
        }
    }
}
