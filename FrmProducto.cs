﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmProducto : Form
    {

        DataTable dtProductos;
        DataRow drProductos;

        public DataTable DtProductos { get => dtProductos; set => dtProductos = value; }
        public DataRow DrProductos {
            set
            {
                drProductos = value;
                txtMarca.Text = drProductos["Marca"].ToString();
                txtModelo.Text = drProductos["Modelo"].ToString();
                cmbColor.SelectedItem = drProductos["Color"].ToString();
                cmbTipo.SelectedItem = drProductos["Tipo"].ToString();
                txtDescripcion.Text = drProductos["Cantidad"].ToString();
                txtPrecio.Text = drProductos["Precio"].ToString();
                
            }
        }

        public FrmProducto()
        {
            InitializeComponent();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            string marca, modelo,  color, tipo, descripcion;
            double precio, cantidad;

            marca = txtMarca.Text;
            modelo = txtModelo.Text;
            color = cmbColor.SelectedItem.ToString();
            tipo = cmbTipo.SelectedItem.ToString();
            cantidad = Double.Parse(txtDescripcion.Text);
            precio = Double.Parse(txtPrecio.Text);
            descripcion = marca + " " + modelo + " " + color;
            if(drProductos != null)
            {
                DataRow drNuevo = DtProductos.NewRow();

                int indice = dtProductos.Rows.IndexOf(drProductos);
                drNuevo["Id"] = drProductos["Id"];
                drNuevo["Marca"] = marca;
                drNuevo["Modelo"] = modelo;
                drNuevo["Color"] = color;
                drNuevo["Tipo"] = tipo;
                drNuevo["Cantidad"] = descripcion;
                drNuevo["Precio"] = precio;
                drNuevo["Descripcion"] = descripcion;
                DtProductos.Rows.RemoveAt(indice);
                DtProductos.Rows.InsertAt(drNuevo, indice);
                DtProductos.Rows[indice].AcceptChanges();
                DtProductos.Rows[indice].SetModified();
            }
            else
            {
                dtProductos.Rows.Add(dtProductos.Rows.Count + 1, marca, modelo, color, tipo, 
                    cantidad, precio, descripcion);
            }
            Dispose();
        }
    }
}
