﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVOS
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsClientes;
        private BindingSource bsClientes;

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataSet DsClientes { get => dsClientes; set => dsClientes = value; }
        public BindingSource BsClientes { get => bsClientes; set => bsClientes = value; }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Cliente"].TableName;
            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true;
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsClientes.Filter = string.Format("Nombre like '*{0}*' or Apellido like '*{0}*'",
                    txtBuscar.Text);
            }
            catch (InvalidExpressionException)
            {

            }
        }

        private void TxtNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.DtClientes = dsClientes.Tables["Cliente"];
            fc.Show();
        }

        private void TxtEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvClientes.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Para editar un cliente, primero debe seleccionarlo.",
                    "Mensaje de error", MessageBoxButtons.OK);
                return;
            }

            DataGridViewRow fila = seleccionados[0];
            DataRow drEdita = ((DataRowView)fila.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.DtClientes = dsClientes.Tables["Cliente"];
            fc.DrCliente = drEdita;
            fc.Show();
        }

        private void TxtCerrar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Eliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvClientes.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Para eliminar un cliente, primero debe seleccionarlo",
                    "Mensaje de error", MessageBoxButtons.OK);
                return;
            }

            DataGridViewRow fila = seleccionados[0];

            DataRow drEliminar = ((DataRowView)fila.DataBoundItem).Row;

            DialogResult respuesta = MessageBox.Show(this, "¿Está seguro de eliminar al cliente?",
                "Mensaje de confirmación", MessageBoxButtons.OKCancel);

            if(respuesta == DialogResult.OK)
            {
                DsClientes.Tables["Cliente"].Rows.Remove(drEliminar);
            }
        }
    }
}
